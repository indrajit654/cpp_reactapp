import React from 'react';

import './modal.css';

const modal = (props) => {
    return (
        <div>
            <div className="modal-wrapper"
                style={{
                    transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}>
                <div className="modal-header">
                    <h3>{props.data.title}</h3>
                    <span className="close-modal-btn" onClick={props.close}>×</span>
                </div>
                <div className="modal-body">
                    <p>
                        {props.children}
                    </p>
                    <p>{props.data.by}</p>
                    <p>{props.data.blurb}</p>
                    <p>backer:{props.data.backers}</p>
                    <p>amountpleadged:{props.data.amountpleadge}</p>
                </div>
            </div>
        </div>
    )
}

export default modal;

