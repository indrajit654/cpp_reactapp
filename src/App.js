import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import getSymbolFromCurrency from 'currency-symbol-map'
import Modal from './component/modal';


class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      error: false,
      isShowing: false,
      userdata:{}
    }
    this.openModalHandler = this.openModalHandler.bind(this)
  }
  componentDidMount() {
    const url = "/kickstarter.json";
    axios.get(url)
      .then((result) => {
        this.setState({
          data: result.data
        })

        console.log("dddd", this.state.data)
      }).catch((err) => {
        this.setState({
          error: true
        })
      })

  }

  openModalHandler = (data) => {
    console.log(data,"lll")
    this.setState({
      isShowing: true,
      userdata:data
    });
    
  }

  closeModalHandler = () => {
    this.setState({
      isShowing: false
    });
  }

  render() {
    return (
      <div>
        <Modal
          className="modal"
          show={this.state.isShowing}
          close={this.closeModalHandler}
          data={this.state.userdata}>
          </Modal>
        <table id="customers" >
          <thead>
            <tr>
              <th>S.no</th>
              <th>Percentage funded</th>
              <th>amount pledged</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((data1, index) => {
              return (
                <tr onClick={()=>this.openModalHandler({title:data1['title'],by:data1['by'],blurb:data1['blurb'],backers:data1['num.backers'],amountpleadge:data1['amt.pledged']})}>
                  <td>{data1['s.no']}</td>
                  <td>{data1['percentage.funded']}</td>
                  <td>{getSymbolFromCurrency(data1['currency']) + data1['amt.pledged']}</td>
                </tr>
              )
            })}

          </tbody>
        </table>
      </div>
    )
  }
}

export default App;
